//
//  ViewController.swift
//  Parsing JSON with Decodable
//
//  Created by Nahin Ahmed on 18.07.19.
//  Copyright © 2019 NAhmed. All rights reserved.
//

import UIKit

struct WebsiteDescription: Decodable {
    let name: String?
    let description: String?
    let courses: [Courses]?
}

struct Courses:Decodable {
    
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let number_of_lessons: Int?
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/website_description"
        guard let url = URL(string: urlString) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            
            do {
                guard let data = data else {
                    return
                }
                
                let websiteDescription = try JSONDecoder().decode(WebsiteDescription.self, from: data)
                
                guard let name = websiteDescription.name else {
                    return
                }
                
                guard let description = websiteDescription.description else {
                    return
                }
                
                guard let courses = websiteDescription.courses else {
                    return
                }
                
                print(name, description)
                
                for course in courses {
                    print(course.id)
                }
                
            } catch let err {
                print(err)
            }
            
        }.resume()
    }
}

